import external from '../externalModules.js';
import pointInsideBoundingBox from '../util/pointInsideBoundingBox.js';

/**
 * Returns the first handle found to be near the provided point. Handles to search can be an array of handles, an
 * object of named handles, or an object of named handles AND named arrays of handles.
 *
 * @public
 * @function getHandleNearImageLine
 * @memberof Manipulators
 *
 * @param {*} element - Target enabledElement
 * @param {(Array|Object)} handles - An arry of handles, object with named handles, or object with named handles AND named arrays of handles
 * @param {Object} coords - The coordinates to measure from when determining distance from handles
 * @param {number} distanceThreshold - minimum distance handle needs to be from provided coords
 * @returns {Object} Handle
 */
const getHandleNearImageLine = function(
  element,
  handles,
  coords,
  distanceThreshold
) {
  let nearbyHandle;

  if (!handles) {
    return;
  }

  if (Array.isArray(handles)) {
    for (let l = 0; l < handles.length; l++) {
      const handleKeys = Object.keys(handles[l]);

      for (let i = 0; i < handleKeys.length; i++) {
        const key = handleKeys[i];
        const handle = handles[l][key];

        if (
          // Not a true handle
          !handle.hasOwnProperty('x') ||
          !handle.hasOwnProperty('y')
        ) {
          continue;
        }

        if (
          _isHandleNearImageFragmentPoint(
            handle,
            element,
            coords,
            distanceThreshold
          )
        ) {
          nearbyHandle = handle;
          break;
        }
      }
      if (nearbyHandle) {
        break;
      }
    }
  } else if (typeof handles === 'object') {
    const handleKeys = Object.keys(handles);

    for (let i = 0; i < handleKeys.length; i++) {
      const handleName = handleKeys[i];

      if (Array.isArray(handles[handleName])) {
        nearbyHandle = getHandleNearImageLine(
          element,
          handles[handleName],
          coords,
          distanceThreshold
        );
        if (nearbyHandle) {
          break;
        }
      } else {
        const handle = handles[handleName];

        if (
          _isHandleNearImageFragmentPoint(
            handle,
            element,
            coords,
            distanceThreshold
          )
        ) {
          nearbyHandle = handle;
          break;
        }
      }
    }
  }

  return nearbyHandle;
};

const _isHandleNearImageFragmentPoint = function(
  handle,
  element,
  coords,
  distanceThreshold
) {
  if (handle.hasOwnProperty('pointNearHandle')) {
    if (handle.pointNearHandle(element, handle, coords)) {
      return true;
    }
  } else if (handle.hasBoundingBox === true) {
    if (pointInsideBoundingBox(handle, coords)) {
      return true;
    }
  } else {
    const handleCanvas = external.cornerstone.pixelToCanvas(element, handle);
    const distance = external.cornerstoneMath.point.distance(
      handleCanvas,
      coords
    );

    if (distance <= distanceThreshold) {
      return true;
    }
  }

  return false;
};

export default getHandleNearImageLine;
