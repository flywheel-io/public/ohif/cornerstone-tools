// SUT
import getMedianAndIQRValues from './getMedianAndIQRValues.js';

describe('util: getMedianAndIQRValues.js', () => {
  it('returns the median and interQuartileRange vales', () => {
    // prettier-ignore
    const fakePixels =  [
      0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
      0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
      0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
      0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
      0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
      0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
      0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
      0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
      0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
      0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
    ];

    const returnValue = getMedianAndIQRValues(fakePixels);

    expect(returnValue.median).toEqual(5);
    expect(returnValue.interQuartileRange).toEqual(6);
  });
});
