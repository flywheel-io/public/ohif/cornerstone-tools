import external from '../../externalModules';

export default function(center, circumferencePoint, image, element) {
  const cornerstoneMath = external.cornerstoneMath;
  const pixelToCanvas = external.cornerstone.pixelToCanvas;
  const canvasToPixel = external.cornerstone.canvasToPixel;
  const centerOnCanvas = pixelToCanvas(element, center);
  const minXEdge = pixelToCanvas(element, {
    x: 0,
    y: center.y,
  });
  const minYEdge = pixelToCanvas(element, {
    x: center.x,
    y: 0,
  });
  const maxXEdge = pixelToCanvas(element, {
    x: image.width,
    y: center.y,
  });
  const maxYEdge = pixelToCanvas(element, {
    x: center.x,
    y: image.height,
  });

  const circumferencePointOnCanvas = pixelToCanvas(element, circumferencePoint);

  const radius = Math.min(
    cornerstoneMath.point.distance(centerOnCanvas, circumferencePointOnCanvas),
    cornerstoneMath.point.distance(centerOnCanvas, minXEdge),
    cornerstoneMath.point.distance(centerOnCanvas, minYEdge),
    cornerstoneMath.point.distance(centerOnCanvas, maxXEdge),
    cornerstoneMath.point.distance(centerOnCanvas, maxYEdge)
  );

  const vector = new cornerstoneMath.Vector3(
    circumferencePointOnCanvas.x - centerOnCanvas.x,
    circumferencePointOnCanvas.y - centerOnCanvas.y,
    0
  );
  const normalizedVector = vector.normalize();
  const vectorTowardsMouseAtRadius = normalizedVector.multiplyScalar(radius);

  circumferencePointOnCanvas.x =
    vectorTowardsMouseAtRadius.x + centerOnCanvas.x;
  circumferencePointOnCanvas.y =
    vectorTowardsMouseAtRadius.y + centerOnCanvas.y;

  const circumferencePointOnImage = canvasToPixel(
    element,
    circumferencePointOnCanvas
  );

  circumferencePoint.x = circumferencePointOnImage.x;
  circumferencePoint.y = circumferencePointOnImage.y;
}
