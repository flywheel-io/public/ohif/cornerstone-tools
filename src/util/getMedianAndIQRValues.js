/**
 *  @private
 *  @function getMedianAndIQRValues
 *  @param {number[]} pixels - Array of the image data's pixel values.
 *  @returns {Object} { median, interQuartileRange }
 */

function getQuartileValue(pixels, index) {
  const QuartileValue =
    pixels.length % 2 ? pixels[index] : (pixels[index - 1] + pixels[index]) / 2;
  return QuartileValue;
}

export default function(pixels) {
  let median = 0.0;
  let interQuartileRange = 0.0;

  if (pixels.length > 0) {
    const MiddleValue = Math.floor(pixels.length / 2);
    pixels.sort((a, b) => a - b);
    median =
      pixels.length % 2
        ? pixels[MiddleValue]
        : (pixels[MiddleValue - 1] + pixels[MiddleValue]) / 2;
    const pixelsLeftSide = pixels.slice(0, MiddleValue);
    const pixelsRightSide =
      pixels.length % 2
        ? pixels.slice(MiddleValue + 1, pixels.length)
        : pixels.slice(MiddleValue, pixels.length);

    if (pixelsLeftSide.length > 0 && pixelsRightSide.length > 0) {
      // First Quartile  Q1 = Median of first half of the pixels array
      const leftSideMiddleValue = Math.floor(pixelsLeftSide.length / 2);
      const FirstQuartileValue = getQuartileValue(
        pixelsLeftSide,
        leftSideMiddleValue
      );
      // Upper Quartile Q3 = Median of second half of the pixels array
      const rightSideMiddleValue = Math.floor(pixelsRightSide.length / 2);
      const UpperQuartileValue = getQuartileValue(
        pixelsRightSide,
        rightSideMiddleValue
      );

      // Inter quartile Range = Q3 - Q1
      interQuartileRange = UpperQuartileValue - FirstQuartileValue;
    }
  }

  return { median, interQuartileRange };
}
