let shouldPreventScroll = () => false;

/**
 * Prevent scrolls through the stack. Validator function for scrolling.
 * @export @public @method
 * @name setScrollValidator
 * @param  {type} preventScroll A function for setting scroll validator.
 * @returns {void}
 */
export function setScrollValidator(preventScroll) {
  shouldPreventScroll = preventScroll;
}

/**
 * Get scroll validator function.
 * @export @public @method
 * @name getScrollValidator
 * @returns {boolean}
 */
export function getScrollValidator() {
  return shouldPreventScroll;
}

export default setScrollValidator;
