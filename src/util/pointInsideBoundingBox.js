import external from '../externalModules.js';

/**
 * Returns true if a point is enclosed within a bounding box.
 * @export @public @method
 * @name pointInsideBoundingBox
 *
 * @param  {Object} handle The handle containing the boundingBox.
 * @param  {Object} coords The coordinate to check.
 * @returns {boolean} True if the point is enclosed within the bounding box.
 */
export default function(handle, coords) {
  // isVisible is set as false when the measurement text box is hidden in viewport.
  if (!handle.boundingBox || handle.isVisible === false) {
    return;
  }

  return external.cornerstoneMath.point.insideRect(coords, handle.boundingBox);
}
