import external from '../../externalModules.js';
import calculateSUV from '../calculateSUV.js';
import calculateEllipseStatistics from './calculateEllipseStatistics.js';

/**
 *
 *
 * @param {*} image
 * @param {*} element
 * @param {*} boundCoordinates
 * @param {*} modality
 * @param {*} pixelSpacing
 * @returns {Object} The Stats object
 */
export default function(
  image,
  element,
  boundCoordinates,
  modality,
  pixelSpacing
) {
  // Retrieve the array of pixels that the annoation bounds cover
  const pixels = external.cornerstone.getPixels(
    element,
    boundCoordinates.left,
    boundCoordinates.top,
    boundCoordinates.width,
    boundCoordinates.height
  );

  // Calculate the mean & standard deviation from the pixels and the annotation details.
  const meanStdDev = calculateEllipseStatistics(pixels, boundCoordinates);

  let meanStdDevSUV;

  if (modality === 'PT') {
    meanStdDevSUV = {
      mean: calculateSUV(image, meanStdDev.mean, true) || 0,
      stdDev: calculateSUV(image, meanStdDev.stdDev, true) || 0,
      median: calculateSUV(image, meanStdDev.median, true) || 0,
      interQuartileRange:
        calculateSUV(image, meanStdDev.interQuartileRange, true) || 0,
    };
  }

  // Calculate the image area from the annotation dimensions and pixel spacing
  const area =
    Math.PI *
    ((boundCoordinates.width * (pixelSpacing.colPixelSpacing || 1)) / 2) *
    ((boundCoordinates.height * (pixelSpacing.rowPixelSpacing || 1)) / 2);

  return {
    area: area || 0,
    count: meanStdDev.count || 0,
    mean: meanStdDev.mean || 0,
    variance: meanStdDev.variance || 0,
    stdDev: meanStdDev.stdDev || 0,
    min: meanStdDev.min || 0,
    max: meanStdDev.max || 0,
    median: meanStdDev.median || 0,
    interQuartileRange: meanStdDev.interQuartileRange || 0,
    meanStdDevSUV,
  };
}
