import calculateEllipseStatistics from './calculateEllipseStatistics.js';
import calculateStatistics from './calculateStatistics.js';
import pointInEllipse from './pointInEllipse.js';

// Named
export { calculateEllipseStatistics, calculateStatistics, pointInEllipse };

// Default
export default {
  calculateEllipseStatistics,
  calculateStatistics,
  pointInEllipse,
};
