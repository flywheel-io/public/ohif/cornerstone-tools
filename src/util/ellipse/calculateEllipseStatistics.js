import pointInEllipse from './pointInEllipse.js';
import getMedianAndIQRValues from './../../util/getMedianAndIQRValues';

/**
 * Calculates the statistics of an elliptical region of interest.
 *
 * @private
 * @function calculateEllipseStatistics
 *
 * @param {number[]} sp - Array of the image data's pixel values.
 * @param {Object} ellipse - { top, left, height, width } - An object describing the ellipse.
 * @returns {Object} { count, mean, variance, stdDev, min, max }
 */
export default function(sp, ellipse) {
  let sum = 0;
  let sumSquared = 0;
  let count = 0;
  let index = 0;
  let min = null;
  let max = null;
  const spValues = [];

  // Since taking the boundary box points, the bounding box points might not come inside the circle
  // when the total width or height are closer to 1px radius

  let centerYAdjustment = 0;
  let centerXAdjustment = 0;

  // If all bounting points are outside the circle ,the center point of the circle is taken.

  if (ellipse.height < 2) {
    centerYAdjustment = ellipse.height / 2;
  }
  if (ellipse.width < 2) {
    centerXAdjustment = ellipse.width / 2;
  }

  for (let y = ellipse.top; y < ellipse.top + ellipse.height; y++) {
    for (let x = ellipse.left; x < ellipse.left + ellipse.width; x++) {
      const point = {
        x: x + centerXAdjustment,
        y: y + centerYAdjustment,
      };

      if (pointInEllipse(ellipse, point)) {
        if (min === null) {
          min = sp[index];
          max = sp[index];
        }
        spValues.push(sp[index]);
        sum += sp[index];
        sumSquared += sp[index] * sp[index];
        min = Math.min(min, sp[index]);
        max = Math.max(max, sp[index]);
        count++;
      }

      index++;
    }
  }

  if (count === 0) {
    return {
      count,
      mean: 0.0,
      variance: 0.0,
      stdDev: 0.0,
      median: 0.0,
      interQuartileRange: 0.0,
      min: 0.0,
      max: 0.0,
    };
  }

  const mean = sum / count;
  const variance = sumSquared / count - mean * mean;
  const { median, interQuartileRange } = getMedianAndIQRValues(spValues);

  return {
    count,
    mean,
    variance,
    stdDev: Math.sqrt(variance),
    median,
    interQuartileRange,
    min,
    max,
  };
}
