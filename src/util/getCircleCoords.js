import external from '../externalModules';

/**
 * Retrieve the bounds of the circle in image coordinates.
 * Handles non-square pixel spacing of images and rotated images.
 *
 * @param {*} startHandle
 * @param {*} endHandle
 * @param {*} element
 * @returns {{ left: number, top: number, width: number, height: number }}
 */
export default function getCircleCoords(startHandle, endHandle, element) {
  const { distance } = external.cornerstoneMath.point;

  const center = external.cornerstone.pixelToCanvas(element, startHandle);
  const originCanvas = external.cornerstone.pixelToCanvas(element, {
    x: 0,
    y: 0,
  });
  const xAxisPointCanvas = external.cornerstone.pixelToCanvas(element, {
    x: 1,
    y: 0,
  });
  const yAxisPointCanvas = external.cornerstone.pixelToCanvas(element, {
    x: 0,
    y: 1,
  });

  const xDirCanvas = new external.cornerstoneMath.Vector3(
    xAxisPointCanvas.x - originCanvas.x,
    xAxisPointCanvas.y - originCanvas.y,
    0
  ).normalize();

  const yDirCanvas = new external.cornerstoneMath.Vector3(
    yAxisPointCanvas.x - originCanvas.x,
    yAxisPointCanvas.y - originCanvas.y,
    0
  ).normalize();

  const pointOnCircumference = external.cornerstone.pixelToCanvas(
    element,
    endHandle
  );
  const radius = distance(center, pointOnCircumference);

  const radiusXDirCanvas = xDirCanvas.multiplyScalar(radius);
  const radiusYDirCanvas = yDirCanvas.multiplyScalar(radius);

  const topLeft = external.cornerstone.canvasToPixel(element, {
    x: center.x - radiusXDirCanvas.x - radiusYDirCanvas.x,
    y: center.y - radiusXDirCanvas.y - radiusYDirCanvas.y,
  });
  const bottomRight = external.cornerstone.canvasToPixel(element, {
    x: center.x + radiusXDirCanvas.x + radiusYDirCanvas.x,
    y: center.y + radiusXDirCanvas.y + radiusYDirCanvas.y,
  });

  return {
    left: topLeft.x,
    top: topLeft.y,
    width: bottomRight.x - topLeft.x,
    height: bottomRight.y - topLeft.y,
  };
}
