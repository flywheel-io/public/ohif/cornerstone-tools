import getBoundaries from './getBoundaries';

/**
 * thresholdfloodFill.js
 * Refactored to ES6.
 *
 * @param {function} getter The getter to the elements of your data structure,
 *                          e.g. getter(x,y) for a 2D interprettation of your structure.
 * @param {number[]} seed The seed for your fill. The dimensionality is infered
 *                        by the number of dimensions of the seed.
 * @param {function} [options.equals] An optional equality method for your datastructure.
 *                            Default is simply value1 = value2.
 * @returns {Object}
 */
export default function(getter, seed, options = {}) {
  const equals = options.equals || defaultEquals;
  const image = options.image || {};
  const seedValue = getter(seed[0], seed[1]);

  const getKeyByIndex = index => `${index[0]}_${index[1]}`;
  const getIndexByKey = key => key.split('_').map(i => parseInt(i, 10));

  const pixelsToProcess = new Set();
  pixelsToProcess.add(getKeyByIndex(seed));

  const flooded = [];
  const visited = {};
  const bounds = [];

  process();

  const boundaries = getBoundaries(flooded);

  return {
    flooded,
    boundaries,
  };

  function process() {
    const pixelIterator = pixelsToProcess[Symbol.iterator]();
    let pixelKey = undefined;

    while ((pixelKey = pixelIterator.next().value)) {
      const index = getIndexByKey(pixelKey);
      flooded.push(index);

      const neighbors = getUnvisitedNeighbours(index);

      for (let i = 0; i < neighbors.length; i++) {
        const neighbourKey = getKeyByIndex(neighbors[i]);

        visited[neighbourKey] = true;
        if (isInRange(neighbors[i])) {
          if (!pixelsToProcess.has(neighbourKey)) {
            pixelsToProcess.add(neighbourKey);
          }
        } else {
          bounds[neighbourKey] = neighbors[i];
        }
      }
    }
  }

  /**
   * Returns neighbouring 8 pixels of given pixel in clockwise order
   * if not visited earlier
   * @param {Array<int>} index row and column value of pixel of image
   * @returns {Array<Array<int>>} row and column values of neighbouring pixels
   */
  function getUnvisitedNeighbours(index) {
    const { width, height } = image;
    const row = index[1];
    const column = index[0];

    // find surrounding 8 pixels
    const neighbors = [];
    const push = idx => {
      if (!visited[getKeyByIndex(idx)]) {
        neighbors.push(idx);
      }
    };

    if (row !== 0) {
      if (column !== 0) {
        push([column - 1, row - 1]); // Upper left
      }
      push([column, row - 1]); // Upper middle
      if (column < width) {
        push([column + 1, row - 1]); // Upper right
      }
    }

    if (column < width) {
      push([column + 1, row]); // Right
    }

    if (row !== height - 1) {
      if (column < width) {
        push([column + 1, row + 1]); // Lower right
      }
      push([column, row + 1]); // Lower middle
      if (column !== 0) {
        push([column - 1, row + 1]); // Lower left
      }
    }

    if (column !== 0) {
      push([column - 1, row]); // Left
    }

    return neighbors;
  }

  function isInRange(pixel) {
    return equals(getter(pixel[0], pixel[1]), seedValue);
  }
}

function defaultEquals(a, b) {
  return a === b;
}
