/**
 * getBoundaries.js
 *
 * @param {UInt8Array} flooded The number of pixels flooded
 * @returns {Object}
 */
export default function(flooded) {
  const { sortedEdgePoints, sortedFloodedPixels } = findEdgePoints(flooded);
  const visitedBoundary = new Set();
  const directions = [
    {
      x: -1,
      y: 0,
    }, // Left
    {
      x: -1,
      y: -1,
    }, // TopLeft
    {
      x: 0,
      y: -1,
    }, // TopCenter
    {
      x: 1,
      y: -1,
    }, // TopRight
    {
      x: 1,
      y: 0,
    }, // Right
    {
      x: 1,
      y: 1,
    }, // BottomRight
    {
      x: 0,
      y: 1,
    }, // BottomCenter
    {
      x: -1,
      y: 1,
    }, // BottomLeft
  ];
  const crossBoundaryPoints = [[-1, 0], [1, 0], [0, -1], [0, 1]]; // Vertical and horizontal neighbours

  return {
    boundaries: findBoundaries(),
  };

  function findBoundaries() {
    const boundaries = [];
    const rows = Object.keys(sortedEdgePoints)
      .map(y => parseInt(y, 10))
      .sort((a, b) => a - b);

    for (let i = 0; i < rows.length; i++) {
      const rowKey = `${rows[i]}`;

      let rowItems = sortedEdgePoints[rowKey].left.sort((a, b) => a - b);

      for (let j = 0; j < rowItems.length; j++) {
        if (!visitedBoundary.has(`${rowItems[j]}_${rows[i]}`)) {
          const boundary = traceBoundary(
            rowItems[j],
            rows[i],
            0 /* Start from left pixel of left boundary*/
          );

          if (boundary.length > 0) {
            boundaries.push(boundary);
          }
        }
      }

      rowItems = sortedEdgePoints[rowKey].right.sort((a, b) => a - b);

      for (let j = 0; j < rowItems.length; j++) {
        if (!visitedBoundary.has(`${rowItems[j]}_${rows[i]}`)) {
          const boundary = traceBoundary(
            rowItems[j],
            rows[i],
            4 /* Start from right pixel of right boundary*/
          );
          if (boundary.length > 0) {
            boundaries.push(boundary);
          }
        }
      }
    }

    return boundaries;
  }

  function traceBoundary(x, y, startDir) {
    const boundary = [];
    let location = { x, y, dir: startDir };

    const isStartPoint = point => point.x === x && point.y === y;

    while (location) {
      if (
        visitedBoundary.has(`${location.x}_${location.y}`) &&
        !isStartPoint(location)
      ) {
        const exisitingBoundaryIndex = getExisitingBoundaryIndex(boundary, [
          location.x,
          location.y,
        ]);

        const removedBoundaryPoints = boundary.splice(exisitingBoundaryIndex);

        removedBoundaryPoints.forEach(point => {
          visitedBoundary.delete(`${point[0]}_${point[1]}`);
        });
      }

      visitedBoundary.add(`${location.x}_${location.y}`);
      boundary.push([location.x, location.y]);

      location = getClockwiseNeighbour(location, boundary);

      if (location) {
        if (isStartPoint(location)) {
          // Reached starting point of boundary
          break;
        }

        const previousPoint = getPreviousPointIfPartOfBoundary(
          location,
          boundary
        );

        if (previousPoint) {
          const lastBoundaryPoint = boundary.pop();
          visitedBoundary.delete(
            `${lastBoundaryPoint[0]}_${lastBoundaryPoint[1]}`
          );
          location = {
            x: previousPoint[0],
            y: previousPoint[1],
            dir: location.dir,
          };
          continue;
        }
      }
    }

    return boundary;
  }

  function getPreviousPointIfPartOfBoundary(location, boundary) {
    if (boundary.length > 1) {
      const previousPoint = boundary[boundary.length - 1];
      if (location.x === previousPoint[0] && location.y === previousPoint[1]) {
        return previousPoint;
      }
    }
  }

  function getExisitingBoundaryIndex(boundary, boundaryToCheck) {
    let index = -1;
    boundary.some((a, i) => {
      if (a.length !== boundaryToCheck.length) {
        return false;
      }
      if (
        a.every((b, j) => {
          return b === boundaryToCheck[j];
        })
      ) {
        index = i;
        return true;
      }
    });

    return index;
  }

  function getClockwiseNeighbour(location, boundary) {
    const traceDirs = directions
      .slice(location.dir)
      .concat(directions.slice(0, location.dir));

    let previousPoint = location;
    let backTrackDir = location;

    for (let i = 0; i < traceDirs.length; i++) {
      const pointToCheck = {
        x: location.x + traceDirs[i].x,
        y: location.y + traceDirs[i].y,
      };

      const key = `${pointToCheck.x}_${pointToCheck.y}`;

      if (
        sortedFloodedPixels.has(key) &&
        isUnvisitedExceptBoundary(pointToCheck, boundary) &&
        hasUnfloodedNeighbourAvailable(pointToCheck)
      ) {
        const dir = {
          x: previousPoint.x - pointToCheck.x,
          y: previousPoint.y - pointToCheck.y,
        };
        backTrackDir = directions.find(d => d.x === dir.x && d.y === dir.y);
        return { ...pointToCheck, dir: directions.indexOf(backTrackDir) };
      }

      previousPoint = pointToCheck;
    }
    // Nothing to return. No points in neighbourhood
  }

  function hasUnfloodedNeighbourAvailable(pointToCheck) {
    return !crossBoundaryPoints.every(point =>
      sortedFloodedPixels.has(
        `${point[0] + pointToCheck.x}_${point[1] + pointToCheck.y}`
      )
    );
  }

  function isUnvisitedExceptBoundary(pointToCheck, currentBoundary) {
    const key = `${pointToCheck.x}_${pointToCheck.y}`;
    const isPointInBoundary = () => {
      // Process point in reverse order to find exisiting point is in current boundary
      for (let i = currentBoundary.length - 1; i >= 0; i--) {
        if (
          currentBoundary[i][0] === pointToCheck.x &&
          currentBoundary[i][1] === pointToCheck.y
        ) {
          return true;
        }
      }

      return false;
    };

    return !visitedBoundary.has(key) || isPointInBoundary();
  }
}

function findEdgePoints(flooded) {
  const sortedEdgePoints = {};
  const sortedFloodedPixels = new Set();
  const sortedPoints = {};

  // Find all column values for each row
  for (let i = 0; i < flooded.length; i++) {
    const rowKey = `${flooded[i][1]}`;
    const row = sortedPoints[rowKey] || [];
    row.push(flooded[i][0]);
    if (!sortedPoints[rowKey]) {
      sortedPoints[rowKey] = row;
    }
  }

  // Sort column values for each row
  const rowKeys = Object.keys(sortedPoints);
  for (let i = 0; i < rowKeys.length; i++) {
    const sortedColumns = sortedPoints[rowKeys[i]].sort((a, b) => a - b);

    const left = [];
    const right = [];

    left.push(sortedColumns[0]);
    sortedFloodedPixels.add(`${sortedColumns[0]}_${rowKeys[i]}`);

    for (let j = 1; j < sortedColumns.length - 1; j++) {
      if (sortedColumns[j - 1] + 1 !== sortedColumns[j]) {
        right.push(sortedColumns[j - 1]);
        left.push(sortedColumns[j]);
      }
      sortedFloodedPixels.add(`${sortedColumns[j]}_${rowKeys[i]}`);
    }

    right.push(sortedColumns[sortedColumns.length - 1]);
    sortedFloodedPixels.add(
      `${sortedColumns[sortedColumns.length - 1]}_${rowKeys[i]}`
    );
    sortedEdgePoints[rowKeys[i]] = { left, right };
  }

  return { sortedEdgePoints, sortedFloodedPixels };
}
