import external from '../../externalModules.js';
import { getModule } from '../../store';
import { draw, drawJoinedLines, getNewContext } from '../../drawing';
import { getDiffBetweenPixelData } from '../../util/segmentation';

const { getters, setters } = getModule('segmentation');

/**
 * Render hook: draws the multiple freehand's outline
 *
 * @param {Object} evt Cornerstone.event#cornerstoneimagerendered > cornerstoneimagerendered event
 * @memberof Tools.ThresholdTool
 * @returns {void}
 */
function renderToolData(evt) {
  const eventData = evt.detail;
  const { element } = eventData;
  const color = getters.brushColor(element, true);
  const context = getNewContext(eventData.canvasContext.canvas);
  const freehands = this.handles.points;

  if (!freehands.length) {
    return;
  }

  draw(context, context => {
    for (let i = 0; i < freehands.length; i++) {
      const points = freehands[i];

      if (points.length < 2) {
        continue;
      }
      for (let j = 0; j < points.length; j++) {
        const lines = [...points[j].lines];

        if (j === points.length - 1) {
          // If it's still being actively drawn, keep the last line to
          // The mouse location
          lines.push(points[0]);
        }
        drawJoinedLines(context, element, points[j], lines, {
          color,
        });
      }
    }
  });
}

let toleranceFunction = ctPixelValue => 0;

function setToleranceFunction(toleranceFn) {
  toleranceFunction = toleranceFn;
}

/**
 * Sets the operation data object for applying strategies
 *
 * @private
 * @param {*} evt // mousedown, touchstart
 * @param {*} type // additive,subtrative,boundary points
 * @returns {Object} operationData (empty if not yet set)
 */
function _getOperationData(evt, type) {
  if (!this.handles) {
    this._resetHandles();
  }
  const points = this.handles.points;
  const { element, image, currentPoints } = evt.detail;
  const referencePoint = currentPoints.image;
  const { labelmap2D, labelmap3D } = getters.labelmap2D(element);

  const pixelData = labelmap2D.pixelData;
  const { rows, columns, slope, intercept } = image;
  const imageData = external.cornerstone.getStoredPixels(
    element,
    0,
    0,
    columns,
    rows
  );

  const storedPixelValue =
    imageData[
      Math.floor(referencePoint.y) * columns + Math.floor(referencePoint.x)
    ];

  const ctPixelValue = storedPixelValue * slope + intercept;

  const operationData = {
    points,
    pixelData,
    imageData,
    segmentIndex: labelmap3D.activeSegmentIndex,
    segmentationMixinType: `multiFreehandSegmentationMixin`,
    type,
    tolerance: toleranceFunction(ctPixelValue),
    referencePoint,
  };

  return operationData;
}

/**
 * Sets the start handle point and claims the eventDispatcher event
 *
 * @private
 * @param {*} evt // mousemove
 * @returns {void|null}
 */
function _startOutliningRegion(evt) {
  const { element } = evt.detail;

  const operationData = this._getOperationData(evt, 'boundaryPoints');

  this.applyActiveStrategy(evt, operationData);

  external.cornerstone.updateImage(element);
}

/**
 * Event handler for MOUSE_UP/TOUCH_END during handle drag event loop.
 *
 * @private
 * @method _applyStrategy
 * @param {(CornerstoneTools.event#MOUSE_UP|CornerstoneTools.event#TOUCH_END)} evt Interaction event emitted by an enabledElement
 * @returns {void}
 */
function _applyStrategy(evt) {
  const { element } = evt.detail;

  const { labelmap2D, currentImageIdIndex } = getters.labelmap2D(element);

  const pixelData = labelmap2D.pixelData;
  const previousPixeldata = pixelData.slice();

  const operationData = this._getOperationData(evt, 'additive');

  this.applyActiveStrategy(evt, operationData);

  const operation = {
    imageIdIndex: currentImageIdIndex,
    diff: getDiffBetweenPixelData(previousPixeldata, pixelData),
  };

  setters.pushState(this.element, [operation]);

  // Invalidate the brush tool data so it is redrawn
  setters.updateSegmentsOnLabelmap2D(labelmap2D);
  this._resetHandles();
  external.cornerstone.updateImage(element);
}

/**
 * Sets the start and end handle points to empty objects
 *
 * @private
 * @method _resetHandles
 * @returns {undefined}
 */
function _resetHandles() {
  this.handles = {
    points: [],
  };
}

function _noop(evt) {}

function setPreMouseMoveCallback(evt) {
  this.preMouseMoveCallbackFunction = evt;
}

function _mouseMoveCallback(evt) {
  if (this.preMouseMoveCallbackFunction(evt) !== false) {
    this._startOutliningRegion(evt);
  } else {
    // Clear previously drawn outline
    this.handles.points.length = 0;
  }
}

function _mouseUpCallback(evt) {
  if (this.preMouseUpCallback(evt) !== false) {
    this._applyStrategy(evt);
  }
}

function _touchEndCallback(evt) {
  this.preTouchEndCallback(evt);
  this._applyStrategy(evt);
}

/**
 * @mixin multiFreehandSegmentationMixin - segmentation operations for threshold tool
 * @memberof Mixins
 */
export default {
  mouseMoveCallback: _mouseMoveCallback,
  touchEndCallback: _touchEndCallback,
  mouseUpCallback: _mouseUpCallback,
  preMouseUpCallback: _noop,
  preTouchEndCallback: _noop,
  setPreMouseMoveCallback,
  preMouseMoveCallbackFunction: evt => evt,
  initializeMixin: _resetHandles,
  renderToolData,
  _resetHandles,
  _applyStrategy,
  _startOutliningRegion,
  _getOperationData,
  setToleranceFunction,
  _mouseUpCallback,
};
