import external from '../externalModules.js';
import BaseTool from './base/BaseTool.js';
import { getToolState, removeToolState } from '../stateManagement/toolState.js';
import { state } from '../store/index.js';
import { eraserCursor } from './cursors/index.js';

/**
 * @public
 * @class EraserTool
 * @memberof Tools
 *
 * @classdesc Tool for deleting the data of other Annotation Tools.
 * @extends Tools.Base.BaseTool
 */
export default class EraserTool extends BaseTool {
  constructor(props = {}) {
    const defaultProps = {
      name: 'Eraser',
      supportedInteractionTypes: ['Mouse', 'Touch'],
      svgCursor: eraserCursor,
    };

    super(props, defaultProps);

    this.preMouseDownCallback = this._deleteAllNearbyTools.bind(this);
    this.preTouchStartCallback = this._deleteAllNearbyTools.bind(this);
  }

  _deleteAllNearbyTools(evt, interactionType = 'mouse') {
    const element = evt.detail.element;

    state.tools.forEach(function(tool) {
      const toolState = getToolState(element, tool.name);

      if (toolState) {
        const tools = toolState.data.slice().reverse();
        // Modifying in a foreach? Probably not ideal
        tools.forEach(function(data) {
          const shouldRemoveToolState = shouldRemoveTool(
            evt,
            tool,
            data,
            interactionType
          );
          if (shouldRemoveToolState) {
            removeToolState(element, tool.name, data);
            external.cornerstone.updateImage(element);
          }
        });
      }
    });

    const consumeEvent = true;

    return consumeEvent;
  }
}

function shouldRemoveTool(event, tool, data, interactionType) {
  const coords = event.detail.currentPoints.canvas;
  const element = event.detail.element;
  const shouldRemoveToolState =
    (typeof tool.pointNearTool === 'function' &&
      tool.pointNearTool(element, data, coords, interactionType)) ||
    (typeof tool.pointInsideTool === 'function' &&
      tool.pointInsideTool(element, data, coords, interactionType));
  return shouldRemoveToolState;
}
