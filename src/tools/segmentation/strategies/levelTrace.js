import { thresholdFloodFill } from '../../../util/segmentation';
import clip from '../../../util/clip';

import { getLogger } from '../../../util/logger';

const logger = getLogger('util:segmentation:operations:levelTrace');

/**
 * Using the stroke given, determine which action(s) to perfom:
 * - Stroke starts and ends outside a segmentation: Behaves as an additive threshold tool.
 * - Stroke starts and ends outside a segmentation: Behaves as an boundaryPoints identifier threshold tool.
 * - Stroke starts and ends inside a segmentation: Behaves as an subtractive threshold tool.
 * - Stroke out-in-out: Section is subtracted.
 * - Stroke in-out-in: Section is added.
 *
 * @param  {Object} evt The cornerstone event.
 * @param  {} operationData An object containing the `pixelData` to
 *                          modify, the `segmentIndex` and the `points` array.
 *
 * @returns {null}
 */
export default function levelTrace(evt, operationData) {
  const {
    points,
    pixelData,
    imageData,
    segmentIndex,
    segmentationMixinType,
    type,
    tolerance,
  } = operationData;

  if (segmentationMixinType !== `multiFreehandSegmentationMixin`) {
    logger.error(
      `levelTrace operation requires multiFreehandSegmentationMixin operationData, recieved ${segmentationMixinType}`
    );
  }

  const nodes = snapPointToGrid(evt, operationData);

  // Create binary labelmap with only this segment for calculations of each operation.
  const workingLabelMap = new Uint8Array(pixelData.length);
  const operation = {
    operationType: type,
    nodes,
    tolerance,
  };

  performOperation(
    operation,
    pixelData,
    imageData,
    workingLabelMap,
    segmentIndex,
    evt,
    points
  );
}

/**
 * Snap the freehand points to the labelmap grid and attach a label for each node.
 *
 * @param  {Object[]} points An array of points drawn by the user.
 * @param  {UInt16Array|Float32Array} pixelData The 2D labelmap.
 * @param  {Object} evt The cornerstone event.
 * @returns {Object[]}
 */
function snapPointToGrid(evt, operationData) {
  const { pixelData, referencePoint } = operationData;

  const { image } = evt.detail;
  const cols = image.width;
  const rows = image.height;

  const nodes = [];
  if (!referencePoint) {
    return nodes;
  }

  let x = Math.floor(referencePoint.x);
  let y = Math.floor(referencePoint.y);

  // Clamp within the confines of the image.
  x = clip(x, 0, cols - 1);
  y = clip(y, 0, rows - 1);

  nodes.push({
    x,
    y,
    segment: pixelData[y * cols + x],
  });

  return nodes;
}

/**
 * Performs the given add/subtract/boundaryPoints operation using a modification of the Tobias Heimann Correction Algorithm:
 * The algorithm is described in full length in Tobias Heimann's diploma thesis (MBI Technical Report 145, p. 37 - 40).
 *
 * @param  {Object} operation The operation.
 * @param  {UInt16Array|Float32Array} pixelData The 2D labelmap.
 * @param  {UInt16Array|Float32Array} workingLabelMap A copy of the labelmap for processing purposes.
 * @param  {number} segmentIndex The label of the tool being used.
 * @param  {Object} evt The cornerstone event.
 */
function performOperation(
  operation,
  pixelData,
  imageData,
  workingLabelMap,
  segmentIndex,
  evt,
  points
) {
  const { width: cols, height: rows } = evt.detail.image;
  const { nodes, operationType, tolerance } = operation;

  // Local getters to swap from cornerstone vector notation and flattened array indicies.
  const getPixelIndex = pixelCoord => pixelCoord.y * cols + pixelCoord.x;
  const getPixelCoordinateFromPixelIndex = pixelIndex => ({
    x: pixelIndex % cols,
    y: Math.floor(pixelIndex / cols),
  });

  if (operationType == 'additive') {
    logger.warn('additive operation...');
  } else if (operationType == 'boundaryPoints') {
    logger.warn('boundaryPoints operation...');
  } else {
    logger.warn('subtractive operation...');
  }

  const pixelPath = nodes.slice();

  // Find extent of region for floodfill (This segment + the drawn loop).
  // This is to reduce the extent of the outwards floodfill, which constitutes 99% of the computation.
  const firstPixelOnPath = pixelPath[0];

  const boundingBox = {
    xMin: firstPixelOnPath.x,
    xMax: firstPixelOnPath.x,
    yMin: firstPixelOnPath.y,
    yMax: firstPixelOnPath.y,
  };

  // ...whilst also initializing the workingLabelmap
  for (let i = 0; i < workingLabelMap.length; i++) {
    if (pixelData[i] === segmentIndex) {
      const pixel = getPixelCoordinateFromPixelIndex(i);

      expandBoundingBox(boundingBox, pixel);
      workingLabelMap[i] = 1;
    } else {
      workingLabelMap[i] = 0;
    }
  }

  // Set workingLabelmap pixelPath to 2 to form a
  // Boundary in the working labelmap to contain the flood fills.

  const fistPixelIndex = getPixelIndex(firstPixelOnPath);
  workingLabelMap[fistPixelIndex] = 2;

  clipBoundingBox(boundingBox, rows, cols);

  // Define a getter for the fill routine to access the working label map.
  function getter(x, y) {
    // Check if out of bounds, as the flood filler doesn't know about the dimensions of
    // The data structure. E.g. if cols is 10, (0,1) and (10, 0) would point to the same
    // position in this getter.

    if (x >= cols || x < 0 || y >= rows || y < 0) {
      return;
    }

    return imageData[y * cols + x];
  }

  const range = {
    min: imageData[fistPixelIndex] - tolerance,
    max: imageData[fistPixelIndex] + tolerance,
  };

  function isInRange(currentPixel, firstPixel) {
    return currentPixel >= range.min && currentPixel <= range.max;
  }

  const floodResult = thresholdFloodFill(
    getter,
    [firstPixelOnPath.x, firstPixelOnPath.y],
    {
      equals: isInRange,
      image: evt.detail.image,
    }
  );

  if (operationType == 'boundaryPoints') {
    points.length = 0;
    const boundaries = floodResult.boundaries.boundaries;
    for (let i = 0; i < boundaries.length; i++) {
      const boundary = boundaries[i];
      let boundaryLength = boundary.length - 1;
      let freehandPoints = [];

      for (let j = 0; j < boundaryLength; j++) {
        freehandPoints.push({
          x: boundary[j][0],
          y: boundary[j][1],
          lines: [
            {
              x: boundary[j + 1][0],
              y: boundary[j + 1][1],
            },
          ],
        });
      }

      freehandPoints.push({
        x: boundary[boundaryLength][0],
        y: boundary[boundaryLength][1],
        lines: [],
      });

      points.push(freehandPoints);
    }
  } else {
    fillFromPixel(floodResult, segmentIndex, workingLabelMap, cols);

    if (floodResult.pixelCount === 0) {
      return;
    }

    const replaceValue = operationType == 'additive' ? segmentIndex : 0;

    for (let i = 0; i < workingLabelMap.length; i++) {
      if (workingLabelMap[i] === segmentIndex) {
        pixelData[i] = replaceValue;
      }
    }
  }
}

/**
 * Expands the bounding box if the pixel falls outside it.
 *
 * @param  {Object} boundingBox The bounding box.
 * @param  {Object} pixel The pixel.
 * @returns {null}
 */
function expandBoundingBox(boundingBox, pixel) {
  const { x, y } = pixel;

  if (x < boundingBox.xMin) {
    boundingBox.xMin = x;
  }
  if (x > boundingBox.xMax) {
    boundingBox.xMax = x;
  }
  if (y < boundingBox.yMin) {
    boundingBox.yMin = y;
  }
  if (y > boundingBox.yMax) {
    boundingBox.yMax = y;
  }
}

/**
 * Expands the bounding box by 2 px and then clips it to the image size.
 * @param  {Object} boundingBox The bounding box.
 * @param  {number} rows The number of rows.
 * @param  {number} cols The number of columns.
 * @returns {null}
 */
function clipBoundingBox(boundingBox, rows, cols) {
  // Add a 2px border to stop the floodfill starting out of bounds and exploading.
  const border = 2;

  boundingBox.xMax = Math.min(boundingBox.xMax + border, cols);
  boundingBox.xMin = Math.max(boundingBox.xMin - border, 0);
  boundingBox.yMax = Math.min(boundingBox.yMax + border, rows);
  boundingBox.yMin = Math.max(boundingBox.yMin - border, 0);
}

/**
 * Performs a floodfill from the given pixel to the workingLabelMap.
 * @param  {Object} result The pixel.
 * @param  {number} fillValue The fill value.
 * @param  {UInt8Array} workingLabelMap The working labelmap.
 * @param  {number} cols The number of columns.
 * @returns {number} The number of pixels flooded.
 */
function fillFromPixel(result, fillValue, workingLabelMap, cols) {
  const flooded = result.flooded;

  for (let p = 0; p < flooded.length; p++) {
    const floodedI = flooded[p];
    workingLabelMap[floodedI[1] * cols + floodedI[0]] = fillValue;
  }
}
