import external from '../externalModules.js';
import drawTextBox, { textBoxWidth } from './drawTextBox.js';
import drawLink from './drawLink.js';

/**
 * Draw a link between an annotation to a textBox.
 * @public
 * @method drawLinkedTextBox
 * @memberof Drawing
 *
 * @param {Object} context - The canvas context.
 * @param {HTMLElement} element - The element on which to draw the link.
 * @param {Object} textBox - The textBox to link.
 * @param {Object} text - The text to display in the textbox.
 * @param {Object[]} handles - The handles of the annotation.
 * @param {Object[]} textBoxAnchorPoints - An array of possible anchor points on the textBox.
 * @param {string} color - The link color.
 * @param {number} lineWidth - The line width of the link.
 * @param {number} xOffset - The x offset of the textbox.
 * @param {boolean} yCenter - Vertically centers the text if true.
 * @returns {undefined}
 */
export default function(
  context,
  element,
  textBox,
  text,
  handles,
  textBoxAnchorPoints,
  color,
  lineWidth,
  xOffset,
  yCenter
) {
  const cornerstone = external.cornerstone;

  let textLines = text;

  if (!Array.isArray(text)) {
    textLines = [text];
  }

  let maxTextWidth = 0;
  const padding = 5;

  textLines.forEach(function(t) {
    // Get the text width in the current font
    const width = textBoxWidth(context, t, padding);

    // Find the maximum with for all the text rows;
    maxTextWidth = Math.max(maxTextWidth, width);
  });

  const textCoords = cornerstone.pixelToCanvas(element, textBox);

  if (!textBox.hasMoved) {
    let availableLeftSpace = 0;

    // Find the current available space in the right side
    const availableRightSpace = element.clientWidth - (textCoords.x + xOffset);

    let leftTextCoords = { x: 0, y: 0 };

    if (availableRightSpace < maxTextWidth) {
      let leftX = Number.POSITIVE_INFINITY;
      const skipFactor = 50;

      if (Array.isArray(handles)) {
        // Skip some points if there are too many handle points
        const skip = Math.max(parseInt(handles.length / skipFactor), 1);

        for (let i = 0; i < handles.length; i += skip) {
          leftX = Math.min(leftX, handles[i].x);
        }
      } else {
        leftX = Math.min(handles.start.x, handles.end.x);
      }

      // Get the coordinates of the text on the left side of the viewport
      leftTextCoords = cornerstone.pixelToCanvas(element, {
        x: leftX,
        y: textBox.y,
      });

      availableLeftSpace = leftTextCoords.x - xOffset;
    }
    let textCoordsX = 0;
    const textMargin = 10;

    if (
      availableRightSpace < maxTextWidth &&
      availableLeftSpace + textMargin > availableRightSpace
    ) {
      textCoordsX = leftTextCoords.x - xOffset - maxTextWidth;
    } else {
      textCoordsX = textCoords.x + xOffset;
    }
    textCoords.x = textCoordsX;
  } else {
    textCoords.x += xOffset;
  }

  const options = {
    centering: {
      x: false,
      y: yCenter,
    },
  };

  // Draw the text box
  textBox.boundingBox = drawTextBox(
    context,
    text,
    textCoords.x,
    textCoords.y,
    color,
    options
  );
  if (textBox.hasMoved) {
    // Identify the possible anchor points for the tool -> text line
    const linkAnchorPoints = textBoxAnchorPoints(handles).map(h =>
      cornerstone.pixelToCanvas(element, h)
    );

    // Draw dashed link line between tool and text
    drawLink(
      linkAnchorPoints,
      textCoords,
      textBox.boundingBox,
      context,
      color,
      lineWidth
    );
  }
}
